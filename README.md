# Cee - C Elektronički evaluator #

Cee - C je evaluator koji kompajlira testni primjerak koda. Služi za ocjenjivanje školskih testova.

### Assets: ###

* Bootstrap
* HTML
* CSS
* Javascript
* Angular

### Funkcije: ###

* login
* ispis zadataka 
* odabir testova
* kompajliranje zadataka
* admin 
* ispis logova
* log sustav

### Korišteni modulei: ###

* express
* body parser
* jsonwebtoken
* mySQL
* angular route
* file system
* sweetalert


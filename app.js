const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);

const jwt = require('jsonwebtoken');
const mysql = require('mysql');
const fs = require('fs');
const spawnSync = require('child_process').spawnSync;
const execSync = require('child_process').execSync;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/assets"));

//================================================================

const secret = "yoda_zirafa";

var users = [];
var razredi = [];
var testovi = [];
var zadatci = [];
var data = {};

//================================================================

var connection = mysql.createConnection({
  host: 'localhost',     // skola:
  user: 'root',				   //	ceepp
  password: '',				   //	ceepp
  database: 'ceepp-c-c-evaluator'
});

connection.connect(function (error) {
  if (error) console.log("[SQL] Error connecting to db", error);
  else console.log("[SQL] Successfully connected to database!");
});



var qusers = "SELECT * FROM users;";
connection.query(qusers, function (error, result, fields) {
  if (error != null) console.log("[SQL] Mysql query error", error);
  else {
    users = result;
    console.log('[SQL] Users successfully loaded!');
  }
});



var q2 = "SELECT * FROM testovi;";
connection.query(q2, function (error, result, fields) {
  if (error != null) console.log("[SQL] Mysql query error", error);
  else {
    console.log('[SQL] Tests succcesfully loaded!')
    testovi = result;
  }
});


//================================================================

app.listen(3000, function () {
  console.log('[Express] Listening on port 3000!');
});

//================================================================

app.post('/auth', function (req, res) {
  var logs = [];
  var status = 401;
  var response = { "success": false };

  var currentTestId = req.body.test;
  var currentTest = getTestString(currentTestId);

  var user = getUser(req.body.ime, req.body.prezime, req.body.password, req.body.razred);
  if (user != null) {
    var qlog = "SELECT * FROM log WHERE user_id=?"
    connection.query(qlog, user.id, function (error, result, fields) {
      if (error != null) {
        console.log("[SQL] Mysql query error", error);
      }
      else {
        var token = jwt.sign(user, secret);

        logs = result;
        if (logs.length > 0) response.firstLogin = false;
        else response.firstLogin = true;

        status = 200;
        response.success = true;
        response.userID = user.id;
        res.json(response);
      }
    });
  }
  else {
    res.json(response);
  }
});

app.post('/admin', function (req, res) {
  var status = 401;
  var response = { "success": false };

  var admin = getAdmin(req.body.ime, req.body.password);

  if (admin != null) {
    status = 200;
    response.users = users;
    response.testovi = testovi;
    response.success = true;
    response.adminOn = true;
    response.userID = admin.id;
  }
  res.status(status).json(response);
});


app.post('/admin/logs', function (req, res) {
  var status = 200;
  var response = { success: true };
  var q = "SELECT * FROM log;";
  connection.query(q, function (error, result, fields) {
    if (error != null) {
      response.success = false;
      status = 400;
      console.log("[SQL] Mysql query error", error);
      res.status(status).json(response);
    }
    else {
      response.users = users;
      response.logs = result;
      res.status(status).json(response);
    }
  });
});

app.post('/admin/grades', function (req, res) {
  var grades = [];
  var status = 200;
  var response = { success: true };
  var q = "SELECT * FROM ispiti;";
  connection.query(q, function (error, result, fields) {
    if (error != null) {
      response.success = false;
      status = 400;
      console.log("[SQL] Mysql query error", error);
      res.status(status).json(response);
    }
    else {
      grades = result;
      response.grades = grades;
      res.status(status).json(response);
    }
  });
});

app.get('/data', function (req, res) {
  var response = { "success": false };
  getRazredi();
  response.razredi = razredi;
  response.testovi = testovi;
  response.success = true;
  res.status(200).json(response);
});


app.get('/testoviData', function (req, res) {
  var response = { "success": false };
  connection.query(q2, function (error, result, fields) {
    if (error != null) {
      console.log("[SQL] Mysql query error", error);
    }
    else {
      testovi = result;
    }
  });

  var zadatciQ = "SELECT * FROM zadatci;";
  connection.query(zadatciQ, function (error, result, fields) {
    if (error != null) {
      console.log("[SQL] Mysql query error", error);
    }
    else {
      zadatci = result;
      response.zadatci = zadatci;
      response.testovi = testovi;
      response.success = true;
      res.status(200).json(response);
    }
  });
});

app.post('/test', function (req, res) {
  var testID = req.body.currTest;
  var response = { success: false };
  var status = 200;

  var qtest = "SELECT * FROM zadatci WHERE test_id= ?;";
  connection.query(qtest, [testID], function (error, result, fields) {
    if (error != null) {
      console.log("[SQL] Mysql query error", error);
      status = 400;
    }
    else {
      response.zadatci = result;
      response.currentTestId = testID;
      response.success = true;
      status = 200;
    }
    res.status(status).json(response);
  });
});

app.post('/finishTest', function (req, res) {
  var response = { success: false };
  var stauts = 200;
  var q = "INSERT INTO ispiti SET ?;"
  var data =
    {
      ime: req.body.ime,
      prezime: req.body.prezime,
      ime_testa: req.body.ime_testa,
      bodovi: req.body.bodovi,
      timestamp: getTimestamp()
    }

  connection.query(q, data, function (err, result) {
    if (err != null) {
      console.log("[SQL] MySQL query Error!", err);
      status = 400;
    } else {
      status = 200;
      response.success = true;
    }
  });
  res.status(stauts).json(response);
});

app.post('/addZadatak', function (req, res) {
  var response = { success: false };
  var status = 200;

  var id = req.body.id;
  var naziv = req.body.naziv;
  var tekst = req.body.tekst;
  var ins = req.body.ins;
  var outs = req.body.outs;
  var test_id = req.body.test_id;
  var broj_zadatka = req.body.broj_zadatka;

  if (id == 'null') {
    var q = "INSERT INTO zadatci SET?;"
    var data =
      {
        naziv: naziv,
        tekst: tekst,
        ins: ins,
        outs: outs,
        test_id: test_id,
        broj_zadatka: broj_zadatka
      };

    connection.query(q, data, function (err, result) {
      if (err != null) {
        console.log("[SQL] Mysql query error", err);
        status = 400;
      }
      else {
        data.id = result.insertId;
        response.data = data;
        response.success = true;
        status = 200;
      }
      res.status(status).json(response);
    });

  }
  else {
    var q = 'UPDATE zadatci SET naziv= ?, tekst=?, ins=?, outs=?, test_id=?, broj_zadatka=? WHERE id= ?'
    var data =
      {
        naziv: naziv,
        tekst: tekst,
        ins: ins,
        outs: outs,
        test_id: test_id,
        broj_zadatka: broj_zadatka,
        id: id
      };

    connection.query(q, [naziv, tekst, ins, outs, test_id, broj_zadatka, id], function (err, result) {
      if (err != null) {
        console.log("[SQL] Mysql query error", err);
        response.success = false;
        response.error = err;
      }
      else {
        status = 200;
        response.success = true;
      }
      res.status(status).json(response);
    });
  }
});


app.post('/admin/test/add', function (req, res) {
  var status = 200;
  var response = { success: false };

  var naziv = req.body.naziv;

  var qtest = "INSERT INTO testovi SET?;"
  var data = { naziv: req.body.naziv };
  connection.query(qtest, data, function (err, result) {
    if (err != null) {
      console.log("[SQL] Mysql query error", err);
      status = 400;
    }
    else {
      data.id = result.insertId;
      response.data = data;
      response.success = true;
      status = 200;
    }
    res.status(status).json(response);
  });
});

app.post('/admin/test/edit', function (req, res) {
  var status = 200;
  var response = { success: false };

  var q = 'UPDATE testovi SET naziv = ? WHERE id = ?'

  connection.query(q, [req.body.naziv, req.body.id], function (err, result) {
    if (err != null) {
      console.log("[SQL] Mysql query error", err);
      response.success = false;
      response.error = err;
    }
    else {
      response.data = data;
      status = 200;
      response.success = true;
    }
    res.status(status).json(response);
  });
});

app.post('/admin/test/delete', function (req, res) {
  var status = 200;
  var response = { success: false };

  var q = 'DELETE FROM testovi WHERE id= ?';
  var id = req.body.id;

  connection.query(q, id, function (err, result) {
    if (err != null) {
      console.log("[SQL] Mysql query error", err);
      response.success = false;
      response.error = err;
    }
    else {
      status = 200;
      response.success = true;
    }
    res.status(status).json(response);
  });
});

app.post('/admin/task/delete', function (req, res) {
  var status = 200;
  var response = { success: false };

  var q = 'DELETE FROM zadatci WHERE id= ?';
  var id = req.body.id;

  connection.query(q, id, function (err, result) {
    if (err != null) {
      console.log("[SQL] Mysql query error", err);
      response.success = false;
      response.error = err;
    }
    else {
      status = 200;
      response.success = true;
    }
    res.status(status).json(response);
  });
});

app.post('/log', function (req, res) {
  var response = { success: false };
  var status = 200;
  var q = "INSERT INTO log SET?;";
  var data;

  var ip = req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress;

  if (req.body.logout_time) {
    var currentTime = getCurrentTime();
    var timestamp = getTimestamp();
    data = { logout_time: currentTime, user_id: req.body.user_id, timestamp: timestamp, ip: ip };
  } else {
    var timestamp = getTimestamp();
    data = { user_id: req.body.user_id, timestamp: timestamp, ip: ip };
  }

  connection.query(q, data, function (error, result, fields) {
    if (error != null) {
      console.log("[SQL] Mysql query error", error);
      status = 400;
    }
    else {
      response.success = true;
      status = 200;
    }
    res.status(status).json(response);
  });
});

app.post('/compile', function (req, res) {
  var noError = 1;
  var status = 400;
  var response = { status: 200 };

  var expectedOuts = [];
  var outs = [];

  var ins = req.body.ins;
  var expectedOuts = req.body.outs;
  var zadatakID = req.body.zadatakID;
  var userID = req.body.userID;
  var kod = req.body.kod;

  var fileName = 'zd' + userID + "_" + zadatakID + '.cpp';
  var fileExe = 'zd' + userID + "_" + zadatakID;

  setTimeout(function () {
    fs.writeFile(fileName, kod + "\n", (err) => {
      if (err)
        throw err;
    });
  }, 20);


  setTimeout(function () {
    try {
      execSync("g++ " + fileName + " -o " + fileExe + ".out -lm");
    }
    catch (err) {
      noError = 0;
      response.success = false;
    }


    if (noError == 1) {
      for (var i = 0; i < ins.length; i++) {
        var a = ins[i].toString();


        var fileExeString = "./" + fileExe + ".out";
        var output = spawnSync(fileExeString, { timeout: 250, input: a });
        outs.push(output.stdout.toString());
      }

      status = 200;
      response.outs = outs;
      var bodovi = brojBodova(expectedOuts, outs);
      response.brojBodova = bodovi;
      res.status(status).json(response);


      execSync("rm -f " + fileExe + ".out");
      execSync("rm -f " + fileName);


      var rjesenje = { zadatak_id: zadatakID, user_id: userID, rjesenje: kod, bodovi: bodovi };
      connection.query('INSERT INTO rjesenja SET?', rjesenje, function (err, result) {
        if (err) {
          console.log("[SQL] Mysql query error", err);
        }
      });
    }
    else {
      var bodovi = 0;
      response.brojBodova = bodovi;
      res.json(response);
      var rjesenje = { zadatak_id: zadatakID, user_id: userID, rjesenje: kod, bodovi: bodovi };
      var rjesenje = { zadatak_id: zadatakID, user_id: userID, rjesenje: kod, bodovi: bodovi };

      connection.query('INSERT INTO rjesenja SET?', rjesenje, function (err, result) {
        if (err) {
          console.log("[SQL] Mysql query error", err);
        }
      });
    }
  }, 100);

});

app.post('/getOuts', function (req, res) {
  var noError = 1;
  var status = 400;
  var response = { status: 200 };

  var outs = [];

  var insString = req.body.ins;
  var kod = req.body.kod;
  var ins = insString.split(',');

  fs.writeFile("test.cpp", kod + "\n", (err) => {
    if (err) throw err;
  });

  setTimeout(function () {
    try {
      execSync("g++ test.cpp -lm");
    }
    catch (err) {
      noError = 0;
      response.success = false;
    }
    if (noError == 1) {
      for (var i = 0; i < ins.length; i++) {
        var a = ins[i].toString();
        try {
          var output = spawnSync("./a", { timeout: 150, input: a });
          outs.push(output.stdout.toString());
        }
        catch (err) {
          console.log(err);
        }

      }
      status = 200;
      var outsString = outs.join();
      response.outs = outsString;
      res.status(status).json(response);

      execSync("rm -f a.out");
      execSync("rm -f test.cpp");
    }

  }, 100);
});

app.all('/list', function (req, res) {
  res.sendFile('index.html', { root: __dirname });
});

//------------------

var apiRoute = express.Router();

apiRoute.use(function (req, res, next) {
  var token = req.query.token || req.headers['x-auth-token'];
  if (token) {
    jwt.verify(token, secret, function (err, payload) {
      if (err) {
        return res.status(401).json({ success: false, message: "Krivi token" });
      }
      else {
        next();
      }
    });
  }
  else {
    return res.status(401).json({ success: false, message: "Fali token" });
  }
});

apiRoute.get('/users', function (req, res) {
  res.status(200).json(users);
});

app.use('/api', apiRoute);


//---------------

function encrypt(plaintext) {
  bcrypt.genSalt(saltRounds, function (err, salt) {
    bcrypt.hash(plaintext, salt, function (err, hash) {
      return hash;
    });
  });
}


function getUser(ime, prezime, password, razred) {
  for (var i = 0; i < users.length; i++) {
    if (users[i].ime == ime && users[i].prezime == prezime && bcrypt.compareSync(password, users[i].password) && users[i].razred == razred) {
      return users[i];
    }
  }
  return null;
}

function getRazredi() {
  for (var i = 0; i < users.length; i++) {
    razredi[i] = users[i].razred;
  }
}

function getAdmin(ime, password) {
  for (var i = 0; i < users.length; i++) {
    if (users[i].ime == ime && bcrypt.compareSync(password, users[i].password) && users[i].superuser == 1) {
      return users[i];
    }
  }
  return null;
}

function getTestString(testId) {
  for (var i = 0; i < testovi.length; i++) {
    if (testovi[i].id == testId) {
      return testovi[i].naziv;
    }
  }
  return null;
}

function getCurrentTime() {
  var d = new Date();
  var hours = d.getHours();
  var minutes = d.getMinutes();
  var seconds = d.getSeconds();

  var date = hours + ":" + minutes + ":" + seconds;
  return date;
}

function getTimestamp() {
  var d = new Date();
  var day = d.getDate();
  var month = d.getMonth() + 1;
  var year = d.getFullYear();
  var hours = d.getHours();
  var minutes = d.getMinutes();
  var seconds = d.getSeconds();

  var date = day + "." + month + "." + year + " " + hours + ":" + minutes + ":" + seconds;
  return date;
}

function brojBodova(expectedOuts, outs) {
  var counter = 0;
  for (var i = 0; i < outs.length; i++) {
    if (expectedOuts[i].toString() == outs[i].toString()) {
      counter++;
    }
  };
  return counter;
}

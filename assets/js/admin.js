app.controller("AdminController",
function($rootScope, $scope, $http, $route, $routeParams, $location, $timeout) {
	 
	 this.error = function()
      {
      swal({
          title: "Nemate pristup ruti!",
          text: "Korisnik nije prijavljen! Redirecting to /list.",
          allowEscapeKey: true,
          confirmButtonColor: "#5cb85c",
          confirmButtonText: "OK"
      });
      $timeout(function() {
            $location.path('/list');
          }, 1000);
  		}

  		this.send = function(ime, password) 
		{
			$rootScope.adminOn = false;
			self.ime;
			self.password;

			var data = {ime:ime, password:password};

			$http({
				data: data,
				method: "POST",
				url: "/admin"
			}).then(function successCallback(response){
				if(response.data.success == true)
				{
					console.log(response);
					$rootScope.adminOn = true;
					$rootScope.userID = response.data.userID;
					$rootScope.users = response.data.users;
					$rootScope.testovi = response.data.testovi;
				
					swal({
		  				title: "Prijava uspješna",
		  				allowEscapeKey: true,
		  				type: "success",
		  				confirmButtonColor: "#5cb85c",
		  				confirmButtonText: "OK"
					});

				} else 
				{
					swal({
		  				title: "Prijava neuspješna!",
		  				text: "Krivo uneseni podatci. Pokušaj ponovo.",
		  				type: "error",
		  				allowEscapeKey: true,
		  				confirmButtonColor: "#f66262",
		  				confirmButtonText: "OK"
					});
				}

			}), function errorCallback(response){
				console.log("GRESKA");
				$rootScope.registerSuccess = false;
			}
		}

		this.click = function(view)
		{

			$location.path(view);
		}

});
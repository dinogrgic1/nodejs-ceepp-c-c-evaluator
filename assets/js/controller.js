app.controller('MainController', function($scope, $rootScope, $http, $route, $routeParams, $location, $timeout) 
{
     $scope.$route = $route;
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;

     this.logOut = function(brojBodova)
     {
        var log = {user_id: $rootScope.userID, logout_time: 1, broj_bodova: brojBodova};

        $http({
            data : log,
            method: "POST",
            url: "/log"
          }).then(function successCallback(response)
          {    
          }), function errorCallback(response)
          {
            console.log("GRESKA");
            $rootScope.registerSuccess = false;
          }


        $location.path('/');
        $timeout(function() {
          for (var prop in $rootScope) 
          {
            if (prop.substring(0,1) !== '$') 
            {
              delete $rootScope[prop];
            }
          }
        }, 100);
     }
 });

app.config(function($routeProvider, $locationProvider) 
{
  $routeProvider
   .when('/', 
   {
    templateUrl: 'views/main.html',
    controller: 'MainController',
   })
   .when('/test', 
   {
    templateUrl: 'views/list.html',
    controller: 'LoginController as login',
   })
   .when('/task',
   {
    templateUrl: 'views/task.html',
    controller: 'TaskController as task',
   })
   .when('/admin',
   {
    templateUrl: 'views/admin.html',
    controller: 'AdminController as admin',
   })
   .when('/admin/edit',
   {
    templateUrl: 'views/edit.html',
    controller: 'EditController as edit',
   })
   .when('/admin/logs',
   {
    templateUrl: 'views/logs.html',
    controller: 'LogController as logs',
   })
   .when('/admin/testovi',
   {
    templateUrl: 'views/testovi.html',
    controller: 'TestoviController as test',
   })
   .when('/admin/grades',
   {
    templateUrl: 'views/grades.html',
    controller: 'GradesController as grade',
   })
   .otherwise(
    { 
    redirectTo: '/test' 
    });
});

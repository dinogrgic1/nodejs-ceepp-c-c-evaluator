app.controller("AdminController",
function($scope, $localStorage, $http, $route, $routeParams, $location, $timeout) {
	 $scope.$storage = $localStorage;
	 $localStorage.hTitle = 'Cee - Admin';
	 this.error = function()
      {
      swal({
          title: "Nemate pristup ruti!",
          text: "Korisnik nije prijavljen! Redirecting to /list.",
          allowEscapeKey: true,
          confirmButtonColor: "#5cb85c",
          confirmButtonText: "OK"
      });
      $timeout(function() {
            $location.path('/list');
          }, 1000);
  		}

  	this.send = function(ime, password) 
		{
			$localStorage.adminOn = false; 
			self.ime;
			self.password;

			var data = {ime:ime, password:password};

			$http({
				data: data,
				method: "POST",
				url: "/admin"
			}).then(function successCallback(response){
				if(response.data.success == true)
				{
					$localStorage.adminOn = true;
					$localStorage.users = response.data.users;
					$localStorage.testovi = response.data.testovi;
					$localStorage.userID = response.data.userID;
				
					swal({
		  				title: "Prijava uspješna",
		  				allowEscapeKey: true,
		  				type: "success",
		  				confirmButtonColor: "#5cb85c",
		  				confirmButtonText: "OK"
					});

				} else 
				{
					swal({
		  				title: "Prijava neuspješna!",
		  				text: "Krivo uneseni podatci. Pokušaj ponovo.",
		  				type: "error",
		  				allowEscapeKey: true,
		  				confirmButtonColor: "#f66262",
		  				confirmButtonText: "OK"
					});
				}

			}), function errorCallback(response){
				console.log("GRESKA");
				$localStorage.registerSuccess = false;
			}
		}

		this.click = function(view)
		{
			$location.path(view);
		}

});
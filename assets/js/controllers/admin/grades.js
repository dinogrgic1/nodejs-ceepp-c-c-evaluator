app.controller("GradesController",
function($scope, $localStorage, $http, $route, $routeParams, $location, $timeout) {
	$scope.$storage = $localStorage;
	$localStorage.hTitle = 'Cee - Popis ocijena';
	this.init = function()
	{
		$localStorage.gradesList = [];
		if($localStorage.adminOn != true)
		{
			$location.path('/#/admin');
		}
		$localStorage.rutaNazad = '/#/admin';
		$http({
			method: "POST",
			url: "/admin/grades"
		}).then(function successCallback(response){
			$localStorage.gradesList = response.data.grades;
		}), function errorCallback(response){
			console.log("GRESKA");
		}		
	}
});
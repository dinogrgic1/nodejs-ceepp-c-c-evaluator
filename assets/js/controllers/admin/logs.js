app.controller("LogController",
function($scope, $localStorage, $http, $route, $routeParams, $location, $timeout) 
{
	$scope.$storage = $localStorage;
	$localStorage.hTitle = 'Cee - Logovi';
	this.init = function()
	{
		if($localStorage.adminOn != true)
		{
			$location.path('/#/admin');
		}
		$localStorage.rutaNazad = '/#/admin';
		$http({
			method: "POST",
			url: "/admin/logs"
		}).then(function successCallback(response){
			$localStorage.logsArray = response.data.logs;
			$localStorage.users = response.data.users;
		}), function errorCallback(response){
			console.log("GRESKA");
		}		
	}
});
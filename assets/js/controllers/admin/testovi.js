app.controller("TestoviController",
function($scope, $localStorage, $rootScope, $http, $uibModal, $route, $routeParams, $location, $window) 
{
	$scope.$storage = $localStorage;
	$localStorage.hTitle = 'Cee - Testovi';
	$localStorage.addTestSuccess = false;
	this.init = function()
	{
		if($localStorage.adminOn != true)
		{
			$location.path('/#/admin');
		}

		$localStorage.rutaNazad = "/#/admin";
			$http({
				method: "GET",
				url: "/testoviData"
			}).then(function successCallback(response)
			{
				self.razredi = response.data.razredi;	
				$localStorage.testovi = response.data.testovi;
				$localStorage.zadatci = response.data.zadatci;
			}), function errorCallback(response)
			{
				console.log("GRESKA");
				$localStorage.registerSuccess = false;
			}
	}

	this.addTest = function(naziv)
	{
		$localStorage.addTestSuccess = false;
		var data = {"naziv": naziv};
		$http({
				data: data,
				method: "POST",
				url: "/admin/test/add"
			}).then(function successCallback(response)
			{
				$localStorage.addTestSuccess = true;
				$localStorage.testovi.push(response.data.data);
				self.newtest_naziv = "";
				self.razredi = response.data.razredi;
				var text = "Ispit " + data.naziv + " dodan u bazu!"
				swal(text);			
				swal("Ispit dodan u bazu!");			
			}), function errorCallback(response)
			{
				console.log("GRESKA");
			}
	}

	this.editTest = function(id, naziv)
	{
		var data = {"id": id, "naziv":naziv};
		$http({
				data: data,
				method: "POST",
				url: "/admin/test/edit"
			}).then(function successCallback(response)
			{
				try
				{
					$localStorage.testovi[id-1].naziv = naziv;
					swal("Change success!");
				}
				catch(error)
				{
					swal("Change error test with that ID doesn't exist!")
				}	
			}), function errorCallback(response)
			{
				console.log("GRESKA");
				$localStorage.editTestSuccess = false;
			}
	}


	this.addZadatak = function(id, naziv, tekst, testID, broj_zadatka)
	{
		var inputi = "", outputi = "";
		for(var i = 0; i < $localStorage.io.length; i++)
	  {
	  	if(i == $localStorage.io.length - 1) 
	  	{
	  		var temp1 = $localStorage.io[i].in;
	  		var temp2 = $localStorage.io[i].out;
	  	}
	  	else 
	  	{
	  		var temp1 = $localStorage.io[i].in + ',';
	  		var temp2 = $localStorage.io[i].out + ',';
	  	}
	  	inputi += temp1;
	  	outputi += temp2;
	  }
		if(id == "null")
		{
			var data = {'id':'null', "naziv": naziv, "tekst":tekst, "ins":inputi, 
			"outs":outputi, "test_id": testID, "broj_zadatka": broj_zadatka};
		}
		else
		{
			var data = {'id': id, "naziv": naziv, "tekst":tekst, "ins":inputi, 
			"outs":outputi, "test_id": testID, "broj_zadatka": broj_zadatka};
		}
		$http({
				data: data,
				method: "POST",
				url: "/addZadatak"
			}).then(function successCallback(response)
			{
				if(id == "null")
				{
					$localStorage.zadatci.push(response.data.data);
					swal("Task added!");
				}
				else
				{
					try
					{
					$localStorage.zadatci[id-1].naziv = naziv;
					$localStorage.zadatci[id-1].tekst = tekst;
					$localStorage.zadatci[id-1].ins = inputi;
					$localStorage.zadatci[id-1].outs = outputi;
					$localStorage.zadatci[id-1].test_id = testID;
					$localStorage.zadatci[id-1].broj_zadatka = broj_zadatka;
					swal("Change success!");
					}
					catch(error)
					{
						swal("Change error : zadatak with that ID doesn't exist!")
					}
				}	
			
			}), function errorCallback(response)
			{
				console.log("GRESKA");
				$localStorage.registerSuccess = false;
			}
	}

	this.send = function(kod, ins)
	  {
	   var ins = "";
	   self.kodZadatka = kod;
	   for(var i = 0; i < $localStorage.io.length; i++)
	   {
	   	if(i == $localStorage.io.length - 1) var temp = $localStorage.io[i].in;
	   	else var temp = $localStorage.io[i].in + ',';
	   	ins += temp;
	   }
	   if(self.ins == undefined) 
	   {
	   	self.ins = "0";
	   }
	   var data = {kod : self.kodZadatka, ins:ins};
	       $http({
	         data : data,
	         method: "POST",
	         url: "/getOuts"
	       }).then(function successCallback(response)
	       {
	       	 var io = response.data.outs.split(',');
	       	 for(var i = 0; i < $localStorage.io.length; i++)
	       	 {
	       	 	$localStorage.io[i].out = io[i];
	       	 }
	         $localStorage.outs = response.data.outs;
	         $localStorage.editTaskOuts = response.data.outs;
	        }), function errorCallback(response)
	        {
	          console.log("GRESKA");
	          $localStorage.registerSuccess = false;
	        }
	}

	$scope.addTestModal = function()
	{
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/admin/addTest.html',
     	size: "md"
		});
	}

	$scope.editTestModal = function(id)
	{
		$localStorage.editID = id; 
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/admin/editTest.html',
     	size: "md"
		});
	}

	this.delTest = function(id)
	{
		var data = {"id":id}
		$http({
			data: data,
			method: "POST",
			url: "/admin/test/delete"
		}).then(function successCallback(response)
		{
			delete $localStorage.testovi[data.id - 1];
			swal("Delete success!");
			$route.reload();
		}), function errorCallback(response)
		{
			console.log("GRESKA");
			$localStorage.delTestSuccess = false;
		}
	}

	this.delTask = function(id)
	{
		var data = {"id":id}
		$http({
			data: data,
			method: "POST",
			url: "/admin/task/delete"
		}).then(function successCallback(response)
		{
			delete $localStorage.zadatci[data.id - 1];
			swal("Delete success!");
			$route.reload();
		}), function errorCallback(response)
		{
			console.log("GRESKA");
			$localStorage.delTaskSuccess = false;
		}
	}

	$scope.addTaskModal = function()
	{
		$localStorage.io = [];
		$localStorage.outs = "";
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/admin/addTask.html',
     	size: "md"
		});
	}

	$scope.editTaskModal = function(test_id, id, broj_zadatka,naziv,tekst,ins,outs)
	{
		$localStorage.io = [];
		$localStorage.outs = "";
		$localStorage.editTestID = test_id;
		$localStorage.editTaskID = id;
		$localStorage.editTaskNum = broj_zadatka;
		$localStorage.editTaskNaziv = naziv;
		$localStorage.editTaskTekst = tekst;
		$localStorage.editTaskIns = ins.split(',');
		$localStorage.editTaskOuts = outs.split(',');
		for(var i = 0; i < $localStorage.editTaskIns.length; i++)
		{
			$localStorage.io[i] = {in:$localStorage.editTaskIns[i], out:$localStorage.editTaskOuts[i]} 
		}
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/admin/editTask.html',
     	size: "md"
		});
	}

	this.addIO = function() 
	{
   	$localStorage.io.push({in: '', out: ''})
  };

  this.delIO = function(id)
  {	
  	$localStorage.io.splice(id, 1);
  	$route.reload();
  }

  this.genOut = function(kod, ins, id)
	  {
	   self.kodZadatka = kod;
	   self.ins = ins;
	   var data = {kod : self.kodZadatka, ins:self.ins};
	       $http({
	         data : data,
	         method: "POST",
	         url: "/getOuts"
	       }).then(function successCallback(response)
	       {
	        $localStorage.io[id].out = response.data.outs;
	        }), function errorCallback(response)
	        {
	          console.log("GRESKA");
	          $localStorage.registerSuccess = false;
	        }
	}
});
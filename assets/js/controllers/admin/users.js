app.controller("UsersController",
function($scope, $localStorage, $http, $route, $routeParams, $location, $timeout) {
	$scope.$storage = $localStorage;
	$localStorage.hTitle = 'Cee - Korisnici';
	this.init = function()
	{
		$localStorage.rutaNazad = "/#/admin";
		if($localStorage.adminOn != true)
		{
			$location.path('/#/admin');
		}
	}
});
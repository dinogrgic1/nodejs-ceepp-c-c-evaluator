app.controller('MainController', function ($scope, $localStorage, $http, $route, $routeParams, $location, $timeout, $window) {
  $scope.$storage = $localStorage;

  $scope.$route = $route;
  $scope.$location = $location;
  $scope.$routeParams = $routeParams;

  this.logOut = function () {
    var log = { user_id: $localStorage.userID, logout_time: 1 };
    $http({
      data: log,
      method: "POST",
      url: "/log",
    });
    $route.reload();
    $localStorage.$reset();
    $location.path('/');
  }
});

app.config(function ($routeProvider, $locationProvider) {
  $routeProvider
    .when('/',
    {
      templateUrl: 'views/user/login.html',
      controller: 'LoginController as login',
    })
    .when('/test',
    {
      templateUrl: 'views/user/list.html',
      controller: 'LoginController as login',
    })
    .when('/task',
    {
      templateUrl: 'views/user/task.html',
      controller: 'TaskController as task',
    })
    .when('/admin',
    {
      templateUrl: 'views/admin/admin.html',
      controller: 'AdminController as admin',
    })
    .when('/admin/users',
    {
      templateUrl: 'views/admin/users.html',
      controller: 'UsersController as usc',
    })
    .when('/admin/logs',
    {
      templateUrl: 'views/admin/logs.html',
      controller: 'LogController as logs',
    })
    .when('/admin/testovi',
    {
      templateUrl: 'views/admin/testovi.html',
      controller: 'TestoviController as test',
    })
    .when('/admin/grades',
    {
      templateUrl: 'views/admin/grades.html',
      controller: 'GradesController as grade',
    })
    .otherwise(
    {
      redirectTo: '/test'
    });
});

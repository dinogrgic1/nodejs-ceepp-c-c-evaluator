app.controller("LoginController",
function($scope, $localStorage, $http, $route, $routeParams, $location, $uibModal, $timeout) 
{
	var self = this;	
	$scope.$storage = $localStorage;

	this.init = function()
	{
		$localStorage.rutaNazad = '/#/';
		$http({
			method: "GET",
			url: "/data"
		}).then(function successCallback(response)
		{
			var razredi = response.data.razredi;	
			$localStorage.razredi = razredi.filter(function(elem, index, self) 
			{ 
    			if(elem != "") return index == self.indexOf(elem);
			}		
		)
			$localStorage.testovi = response.data.testovi;
		}), function errorCallback(response)
		{
			$localStorage.dataSuccess = false;
		}
	}

	this.send = function(ime, prezime, password, razred, test) 
	{
		$localStorage.loadingSpinner = 'Logging in';
		$localStorage.loginSuccess = false;
		self.ime, self.prezime, self.password, self.razred, self.test;

		var data = {ime:ime, prezime:prezime, password:password, razred:razred, test:test};

		$http({
			data: data,
			method: "POST",
			url: "/auth"
		}).then(function successCallback(response)
		{
		if(response.data.success == true)
		{
			$localStorage.loginSuccess = true;
			$localStorage.token = response.data.token;
			$localStorage.ime_prezime = response.config.data.ime + " " + response.config.data.prezime;
			$localStorage.ime = response.config.data.ime;
			$localStorage.prezime = response.config.data.prezime;
			$localStorage.userID = response.data.userID;
			$localStorage.superuser = response.data.superuser;
			$localStorage.firstLogin = response.data.firstLogin;
			$localStorage.zadatciBodovi = [];

			for(var i = 0; i < 50; i++) $localStorage.zadatciBodovi[i] = 0;
			$localStorage.zadatciZadnjiKod = [];

			swal({
	  		title: "Upute za test",
	  		text: "Zadatak nosi bodove (max 10) samo ako se uspješno prevede (kompajlira) i ispisuje točna rješenja.\n\nPrilikom ispisa dodati \\n!!.\n\nPrilikom svakog slanja (spremanja) rješenja program se automatski pregleda te se iznad poslanog koda ispisuje broj točnih odgovora.\n\n Sretno!\n\t",
	  		allowEscapeKey: true,
	  		confirmButtonColor: "#5cb85c",
	  		confirmButtonText: "OK"
			});
			$localStorage.loadingSpinner = null;
			$location.path('/test');
		} 
		else 
		{
		$localStorage.loginSuccess = false;
			swal({
	  		title: "Prijava neuspješna!",
	  		text: "Krivo uneseni podatci. Pokušaj ponovo.",
	  		type: "error",
	  		allowEscapeKey: true,
	  		confirmButtonColor: "#f66262",
	  		confirmButtonText: "OK"
			});
			}
			$localStorage.loadingSpinner = null;
		}), function errorCallback(response)
		{
			console.log("GRESKA");
			$localStorage.loginSuccess = false;
		};
	}

	this.salabahter = function(size, template)
	{
		const modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '../views/salabahter.html',
      size: "md"
		});
		return modalInstance;
	}
});

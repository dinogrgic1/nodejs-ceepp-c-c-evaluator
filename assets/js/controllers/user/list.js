app.controller("ListController",
function($scope, $uibModal, $localStorage, $http, $route, $routeParams, $location, $timeout) {
	var self = this;
	$scope.$storage = $localStorage;

	this.init = function()
	{	
		$localStorage.rutaNazad = '/#/test';
		
		if($localStorage.loginSuccess != true)
		{
		swal({
	     	title: "Nemate pristup ruti!",
	      	text: "Korisnik nije prijavljen!",
	      	allowEscapeKey: true,
	      	confirmButtonColor: "#5cb85c",
	      	confirmButtonText: "OK"
	    });
	    $timeout(function() {
	       $location.path('/');
	    }, 10);
		}

		var data = {currTest: $localStorage.testID};

		$http({
			data: data,
			method: "POST",
			url: "/test"
		}).then(function successCallback(response)
		{	
			$localStorage.brojBodova = 0;
			for(var i = 0; i < $localStorage.zadatciBodovi.length; i++)
			{
				$localStorage.brojBodova += $localStorage.zadatciBodovi[i];
			}
			$localStorage.odabraniTest = testName($localStorage.testID);
			$localStorage.hTitle = 'Cee - ' + $localStorage.odabraniTest;
			$localStorage.zadatci = response.data.zadatci;	
			$localStorage.brojZadataka = response.data.zadatci.length;
			$localStorage.brojUkupnihBodova = (response.data.zadatci.length) * 10;
			$localStorage.postotakBodova = ($localStorage.brojBodova / $localStorage.brojUkupnihBodova) * 100;

		}), function errorCallback(response)
		{
			console.log("GRESKA");
			$localStorage.registerSuccess = false;
		}
	}

	this.odaberi = function(zadatak, zadatakID)
	{
		$localStorage.odabraniZadatakID = zadatakID;
		$localStorage.odabraniZadatak = zadatak;

		$location.path('/task');
	}

	this.firstLogin = function()
	{
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/salabahter.html',
     	size: "md"
		});
	}

	this.finishTest = function(ime,prezime,ime_testa,bodovi)
	{
		self.ime, self.prezime, self.ime_testa, self.bodovi;
		var data = {ime:ime,prezime:prezime,ime_testa:ime_testa,bodovi:bodovi};
		$http({
			data : data,
			method: "POST",
			url: "/finishTest"
		}).then(function successCallback(response)
		{	
			var text = $localStorage.brojBodova + "/ " + $localStorage.brojUkupnihBodova;
			swal({
	  				title: "Test poslan!",
	  				text: "Tvoj konačan broj bodova je : " + text,
	  				allowEscapeKey: true,
	  				html : true,
	  				confirmButtonColor: "#f66262",
	  				confirmButtonText: "OK"
				});
		$localStorage.testDone = true;
		}), function errorCallback(response)
		{
			console.log("GRESKA");
		}
	}
	function testName(id)
	{
		for(var i = 0; i < $localStorage.testovi.length; i++)
		{
			if(id == $localStorage.testovi[i].id)
			{
				return $localStorage.testovi[i].naziv;
			}
		}
	}

});


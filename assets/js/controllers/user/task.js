app.controller("TaskController",
function($scope, $localStorage, $http, $route, $routeParams, $location, $timeout) {
  var self = this;
  $scope.$storage = $localStorage;
  this.init = function()
  {
    if($localStorage.loginSuccess != true)
    {
      $location.path('/#/test');
    }
    $localStorage.rutaNazad = '/#/test';
    self.nazivZadatka = $localStorage.odabraniZadatak.naziv;
    self.tekstZadatka = $localStorage.odabraniZadatak.tekst;
    $localStorage.hTitle = 'Cee - ' + self.nazivZadatka;
    self.ins = $localStorage.odabraniZadatak.ins.split(',');
    self.outs = $localStorage.odabraniZadatak.outs.split(',');
    self.odabraniZadatakBodovi = 0;
    
    // Stavljanje testnih primjeraka u varijablu
    $localStorage.testniPrimjeri = "";
    for(var i = 0; i < 3; i++)
    {
      $localStorage.testniPrimjeri += "\n-----------------------\n"+ "input:\n" + self.ins[i] + 
      "\n" + "output:\n"  + self.outs[i] + 
      "\n";
    }
  }


  this.send = function(kod)
  {
    var log = {user_id: $localStorage.userID, logout_time: 0};

    self.kodZadatka = kod;

    $http({
        data : log,
        method: "POST",
        url: "/log"
      }).then(function successCallback(response)
      {   
      }), function errorCallback(response)
      {
        console.log("GRESKA");
        $localStorage.registerSuccess = false;
      }

      var data = {kod : self.kodZadatka, ins:self.ins, outs:self.outs, userID: $localStorage.userID, zadatakID:$localStorage.odabraniZadatakID};
      
      swal({
      title: "Kompajliranje",
      text: 'Molim vas pričekajte... <br><br><span class="glyphicon glyphicon-refresh spinning"></span>',
      showConfirmButton: false,
      confirmButtonColor: "#5cb85c",
      html: true
      },
        $http({
          data : data,
          method: "POST",
          url: "/compile"
        }).then(function successCallback(response)
        {
          $localStorage.zadatciBodovi[$localStorage.odabraniZadatakID] = response.data.brojBodova;
          $localStorage.zadatciZadnjiKod[$localStorage.odabraniZadatakID] = self.kodZadatka;
          var text = "Broj bodova: " + response.data.brojBodova + " /10";
          swal({
            title: "Kompajliranje završeno!", 
            text: text, 
            html: true
          });
  
          }), function errorCallback(response)
          {
            console.log("GRESKA");
            $localStorage.registerSuccess = false;
          });
        }

  this.error = function()
  {
  swal({
      title: "Nemate pristup ruti!",
      text: "Korisnik nije prijavljen!",
      allowEscapeKey: true,
      confirmButtonColor: "#5cb85c",
      confirmButtonText: "OK"
  });
  $timeout(function() {
        $location.path('/');
      }, 1000);
  }
});


app.controller("GradesController",
function($rootScope, $scope, $http, $route, $routeParams, $location, $timeout) {
	this.init = function()
	{
		$rootScope.gradesList = [];
		if($rootScope.adminOn != true)
		{
			$location.path('/#/admin');
		}
		$rootScope.rutaNazad = '/#/admin';
		$http({
			method: "POST",
			url: "/admin/grades"
		}).then(function successCallback(response){
			$rootScope.gradesList = response.data.grades;
		}), function errorCallback(response){
			console.log("GRESKA");
		}		
	}
});
app.controller("ListController",
function($rootScope, $uibModal, $scope, $http, $route, $routeParams, $location, $timeout) {
	var self = this;

	this.init = function()
	{	
		$rootScope.rutaNazad = '/#/test';
		
		if($rootScope.loginSuccess != true)
		{
		swal({
	     	title: "Nemate pristup ruti!",
	      	text: "Korisnik nije prijavljen!",
	      	allowEscapeKey: true,
	      	confirmButtonColor: "#5cb85c",
	      	confirmButtonText: "OK"
	    });
	    $timeout(function() {
	       $location.path('/');
	    }, 10);
		}

		var data;
		$http({
			data : data,
			method: "POST",
			url: "/test"
		}).then(function successCallback(response)
		{	
			$rootScope.brojBodova = 0;
			for(var i = 0; i < $rootScope.zadatciBodovi.length; i++)
			{
				$rootScope.brojBodova += $rootScope.zadatciBodovi[i];
			}
			$rootScope.odabraniTest = response.data.currentTest;
			$rootScope.zadatci = response.data.zadatci;	
			$rootScope.brojZadataka = response.data.zadatci.length;
			$rootScope.brojUkupnihBodova = (response.data.zadatci.length) * 10;
			$rootScope.postotakBodova = ($rootScope.brojBodova / $rootScope.brojUkupnihBodova) * 100;

		}), function errorCallback(response)
		{
			console.log("GRESKA");
			$rootScope.registerSuccess = false;
		}
	}

	this.odaberi = function(zadatak, zadatakID)
	{
		$rootScope.odabraniZadatakID = zadatakID;
		$rootScope.odabraniZadatak = zadatak;

		$location.path('/task');
	}

	this.firstLogin = function()
	{
				$uibModal.open({
      		animation: true,
      		templateUrl: '../views/salabahter.html',
      		size: "md"
				});
	}

});


app.controller("LoginController",
function($rootScope, $uibModal, $scope, $http, $route, $routeParams, $location, $timeout) {
	var self = this;	

	this.init = function()
	{
		$rootScope.rutaNazad = '/#/';
		$http({
			method: "GET",
			url: "/data"
		}).then(function successCallback(response)
		{
			var razredi = response.data.razredi;	
			$rootScope.razredi = razredi.filter(function(elem, index, self) 
			{ 
    			if(elem != "") return index == self.indexOf(elem);
			}		
		)
			$rootScope.testovi = response.data.testovi;
		}), function errorCallback(response)
		{
			$rootScope.registerSuccess = false;
		}
	}

	this.send = function(ime, prezime, password, razred, test) 
	{
		$rootScope.loadingSpinner = 'Logging in';
		$rootScope.loginSuccess = false;
		self.ime, self.prezime, self.password, self.razred, self.test;

		var data = {ime:ime, prezime:prezime, password:password, razred:razred, test:test};

		$http({
			data: data,
			method: "POST",
			url: "/auth"
		}).then(function successCallback(response){
			if(response.data.success == true)
			{
				console.log(response);
				$rootScope.loginSuccess = true;
				$rootScope.token = response.data.token;
				$rootScope.ime_prezime = response.config.data.ime + " " + response.config.data.prezime;
				$rootScope.ime = response.config.data.ime;
				$rootScope.prezime = response.config.data.prezime;
				$rootScope.userID = response.data.userID;
				$rootScope.superuser = response.data.superuser;
				$rootScope.firstLogin = response.data.firstLogin;
				$rootScope.zadatciBodovi = [];

				for(var i = 0; i < 50; i++)
				{
					$rootScope.zadatciBodovi[i] = 0;
				}
				
				$rootScope.zadatciZadnjiKod = [];

				swal({
	  				title: "Upute za test",
	  				text: "Zadatak nosi bodove (max 10) samo ako se uspješno prevede (kompajlira) i ispisuje točna rješenja.\n\nPrilikom ispisa dodati \\n!!.\n\nPrilikom svakog slanja (spremanja) rješenja program se automatski pregleda te se iznad poslanog koda ispisuje broj točnih odgovora.\n\n Sretno!\n\t",
	  				allowEscapeKey: true,
	  				confirmButtonColor: "#5cb85c",
	  				confirmButtonText: "OK"
				});
				$rootScope.loadingSpinner = null;
				$location.path('/test');
			} else 
			{
				swal({
	  				title: "Prijava neuspješna!",
	  				text: "Krivo uneseni podatci. Pokušaj ponovo.",
	  				type: "error",
	  				allowEscapeKey: true,
	  				confirmButtonColor: "#f66262",
	  				confirmButtonText: "OK"
				});
			}
			$rootScope.loadingSpinner = null;

		}), function errorCallback(response){
			console.log("GRESKA");
			$rootScope.registerSuccess = false;
		}
	}

	this.finishTest = function(ime,prezime,ime_testa,bodovi)
	{
		self.ime, self.prezime, self.ime_testa, self.bodovi;
		var data = {ime:ime,prezime:prezime,ime_testa:ime_testa,bodovi:bodovi};
		$http({
			data : data,
			method: "POST",
			url: "/finishTest"
		}).then(function successCallback(response)
		{	
			var text = $rootScope.brojBodova + "/ " + $rootScope.brojUkupnihBodova;
			swal({
	  				title: "Test poslan!",
	  				text: "Tvoj konačan broj bodova je : " + text,
	  				allowEscapeKey: true,
	  				html : true,
	  				confirmButtonColor: "#f66262",
	  				confirmButtonText: "OK"
				});
		$rootScope.testDone = true;
		}), function errorCallback(response)
		{
			console.log("GRESKA");
		}
	}

	this.salabahter = function(size, template)
	{
		const modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '../views/salabahter.html',
      size: "md"
		});

		return modalInstance;
	}

});

app.controller("LogController",
function($rootScope, $scope, $http, $route, $routeParams, $location, $timeout) {
	this.init = function()
	{
		if($rootScope.adminOn != true)
		{
			$location.path('/#/admin');
		}
		$rootScope.rutaNazad = '/#/admin';
		$http({
			method: "POST",
			url: "/admin/logs"
		}).then(function successCallback(response){
			console.log(response);
			$rootScope.logsArray = response.data.logs;
			$rootScope.users = response.data.users;
			console.log($rootScope.users);
		}), function errorCallback(response){
			console.log("GRESKA");
		}		
	}
});
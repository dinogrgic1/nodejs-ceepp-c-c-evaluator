app.controller("TaskController",
function($rootScope, $scope, $http, $route, $routeParams, $location, $timeout) {
  var self = this;

  this.init = function()
  {
    if($rootScope.loginSuccess != true)
    {
      $location.path('/#/test');
    }
    $rootScope.rutaNazad = '/#/test';

    self.nazivZadatka = $rootScope.odabraniZadatak.naziv;
    self.tekstZadatka = $rootScope.odabraniZadatak.tekst;
    self.ins = $rootScope.odabraniZadatak.ins;
    self.outs = $rootScope.odabraniZadatak.outs;
    self.odabraniZadatakBodovi = 0;

  }

  this.send = function(kod)
  {
    var log = {user_id: $rootScope.userID, logout_time: 0};

    self.kodZadatka = kod;

    $http({
        data : log,
        method: "POST",
        url: "/log"
      }).then(function successCallback(response)
      {   
      }), function errorCallback(response)
      {
        console.log("GRESKA");
        $rootScope.registerSuccess = false;
      }

      var data = {kod : self.kodZadatka, ins:self.ins, outs:self.outs, userID: $rootScope.userID, zadatakID:$rootScope.odabraniZadatakID};
      
      swal({
      title: "Kompajliranje",
      text: 'Molim vas pričekajte... <br><br><span class="glyphicon glyphicon-refresh spinning"></span>',
      showConfirmButton: false,
      confirmButtonColor: "#5cb85c",
      html: true
      },
        $http({
          data : data,
          method: "POST",
          url: "/compile"
        }).then(function successCallback(response)
        {
          $rootScope.zadatciBodovi[$rootScope.odabraniZadatakID] = response.data.brojBodova;
          $rootScope.zadatciZadnjiKod[$rootScope.odabraniZadatakID] = self.kodZadatka;
          var text = "Broj bodova: " + response.data.brojBodova + " /10";
          swal({
            title: "Kompajliranje završeno!", 
            text: text, 
            html: true
          });
  
          }), function errorCallback(response)
          {
            console.log("GRESKA");
            $rootScope.registerSuccess = false;
          });
        }

  this.error = function()
      {
      swal({
          title: "Nemate pristup ruti!",
          text: "Korisnik nije prijavljen!",
          allowEscapeKey: true,
          confirmButtonColor: "#5cb85c",
          confirmButtonText: "OK"
      });
      $timeout(function() {
            $location.path('/');
          }, 1000);
  }
});


app.controller("TestoviController",
function($rootScope, $uibModal, $scope, $http, $route, $routeParams, $location, $timeout) {
	//$rootScope.addTestSuccess = false;
	this.init = function()
	{
		if($rootScope.adminOn != true)
		{
			$location.path('/#/admin');
		}

		$rootScope.rutaNazad = "/#/admin";
			$http({
				method: "GET",
				url: "/testoviData"
			}).then(function successCallback(response)
			{
				self.razredi = response.data.razredi;	
				$rootScope.testovi = response.data.testovi;
				$rootScope.zadatci = response.data.zadatci;
			}), function errorCallback(response)
			{
				console.log("GRESKA");
				$rootScope.registerSuccess = false;
			}
	}

	this.addTest = function(naziv)
	{
		$rootScope.addTestSuccess = false;
		var data = {"naziv": naziv};
		$http({
				data: data,
				method: "POST",
				url: "/admin/test/add"
			}).then(function successCallback(response)
			{
				$rootScope.addTestSuccess = true;
				$rootScope.testovi.push(response.data.data);
				self.newtest_naziv = "";
				self.razredi = response.data.razredi;
				var text = "Ispit " + data.naziv + " dodan u bazu!"
				swal(text);			
				swal("Ispit dodan u bazu!");			
			}), function errorCallback(response)
			{
				console.log("GRESKA");
			}
	}

	this.editTest = function(id, naziv)
	{
		var data = {"id": id, "naziv":naziv};
		$http({
				data: data,
				method: "POST",
				url: "/admin/test/edit"
			}).then(function successCallback(response)
			{
				try
				{
					$rootScope.testovi[id-1].naziv = naziv;
					swal("Change success!");
				}
				catch(error)
				{
					swal("Change error test with that ID doesn't exist!")
				}	
			}), function errorCallback(response)
			{
				console.log("GRESKA");
				$rootScope.editTestSuccess = false;
			}
	}


	this.addZadatak = function(id, naziv, tekst, inputi, outputi, testID, broj_zadatka)
	{
		console.log('1');
		if(id == "null")
		{
			var data = {'id':'null', "naziv": naziv, "tekst":tekst, "ins":inputi, 
			"outs":outputi, "test_id": testID, "broj_zadatka": broj_zadatka};
		}
		else
		{
			var data = {'id': id, "naziv": naziv, "tekst":tekst, "ins":inputi, 
			"outs":outputi, "test_id": testID, "broj_zadatka": broj_zadatka};
		}
		$http({
				data: data,
				method: "POST",
				url: "/addZadatak"
			}).then(function successCallback(response)
			{
				if(id == "null")
				{
					$rootScope.zadatci.push(response.data.data);
					swal("Task added!");
				}
				else
				{
					try
					{
					$rootScope.zadatci[id-1].naziv = naziv;
					$rootScope.zadatci[id-1].tekst = tekst;
					$rootScope.zadatci[id-1].ins = inputi;
					$rootScope.zadatci[id-1].outs = outputi;
					$rootScope.zadatci[id-1].test_id = testID;
					$rootScope.zadatci[id-1].broj_zadatka = broj_zadatka;
					swal("Change success!");
					}
					catch(error)
					{
						swal("Change error : zadatak with that ID doesn't exist!")
					}
				}	
			
			}), function errorCallback(response)
			{
				console.log("GRESKA");
				$rootScope.registerSuccess = false;
			}
	}

	this.send = function(kod, ins)
	  {
	   self.kodZadatka = kod;
	   self.ins = ins;
	   if(self.ins == undefined) 
	   {
	   	self.ins = "0";
	   }
	   var data = {kod : self.kodZadatka, ins:self.ins};

	       console.log("!")
	       $http({
	         data : data,
	         method: "POST",
	         url: "/getOuts"
	       }).then(function successCallback(response)
	       {
	         $rootScope.outs = response.data.outs;
	        }), function errorCallback(response)
	        {
	          console.log("GRESKA");
	          $rootScope.registerSuccess = false;
	        }
	}

	$scope.addTestModal = function()
	{
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/addTest.html',
     	size: "md"
		});
	}

	$scope.editTestModal = function(id)
	{
		$rootScope.editID = id; 
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/editTest.html',
     	size: "md"
		});
	}

	this.delTest = function(id)
	{
		var data = {"id":id}
		$http({
			data: data,
			method: "POST",
			url: "/admin/test/delete"
		}).then(function successCallback(response)
		{
			delete $rootScope.testovi[data.id - 1];
			swal("Delete success!");
		}), function errorCallback(response)
		{
			console.log("GRESKA");
			$rootScope.delTestSuccess = false;
		}
	}

	this.delTask = function(id)
	{
		var data = {"id":id}
		$http({
			data: data,
			method: "POST",
			url: "/admin/task/delete"
		}).then(function successCallback(response)
		{
			delete $rootScope.zadatci[data.id - 1];
			swal("Delete success!");
		}), function errorCallback(response)
		{
			console.log("GRESKA");
			$rootScope.delTaskSuccess = false;
		}
	}

	$scope.addTaskModal = function()
	{
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/addTask.html',
     	size: "md"
		});
	}

	$scope.editTaskModal = function(id, broj_zadatka,naziv,tekst,ins,outs)
	{
		$rootScope.editTaskID = id;
		$rootScope.editTaskNum = broj_zadatka;
		$rootScope.editTaskNaziv = naziv;
		$rootScope.editTaskTekst = tekst;
		$rootScope.editTaskIns = ins;
		$rootScope.editTaskOuts = outs;
		$uibModal.open({
     	animation: true,
     	templateUrl: '../views/editTask.html',
     	size: "md"
		});
	}
	

});

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2017 at 01:22 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ceepp-c-c-evaluator`
--

-- --------------------------------------------------------

--
-- Table structure for table `ispiti`
--

CREATE TABLE `ispiti` (
  `id` int(11) NOT NULL,
  `ime` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prezime` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_testa` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bodovi` int(11) DEFAULT NULL,
  `timestamp` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ispiti`
--

INSERT INTO `ispiti` (`id`, `ime`, `prezime`, `ime_testa`, `bodovi`, `timestamp`) VALUES
(1, 'Dino', 'Grgić', 'Primjer Testa', 0, ''),
(2, 'Dino', 'Grgić', 'Primjer Testa', 0, ''),
(3, 'Dino', 'Grgić', 'Primjer Testa', 0, ''),
(4, 'Dino', 'Grgić', 'Primjer Testa', 0, ''),
(5, 'Dino', 'Grgić', 'Primjer Testa', 0, ''),
(6, 'Dino', 'Grgić', 'Primjer Testa', 0, '18:2:31'),
(7, 'Dino', 'Grgić', 'Primjer Testa', 0, '18:3:17'),
(8, 'Dino', 'Grgić', 'Primjer Testa', 0, '18:5:3'),
(9, 'Dino', 'Grgić', 'Primjer Testa', 0, '18:8:32'),
(10, 'Dino', 'Grgić', 'Primjer Testa', 0, '1.0.117@ 18:9:20'),
(11, 'Dino', 'Grgić', 'Primjer Testa', 0, '30.1.2017 18:13:29'),
(12, 'Dino', 'Grgić', 'Primjer Testa', 0, '30.1.2017 18:14:31'),
(13, 'Dunja', 'Šmigovec', 'Primjer Testa', 0, '31.1.2017 22:20:58');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logout_time` time DEFAULT NULL,
  `broj_bodova` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `user_id`, `timestamp`, `ip`, `logout_time`, `broj_bodova`) VALUES
(1, 26, '0000-00-00 00:00:00', '::1', '19:27:49', 0),
(2, 3, '0000-00-00 00:00:00', '::1', '19:39:20', NULL),
(3, 3, '0000-00-00 00:00:00', '::1', '19:42:35', 0),
(4, 3, '0000-00-00 00:00:00', '::1', '19:54:17', 0),
(5, 3, '0000-00-00 00:00:00', '::1', '20:40:43', 0),
(6, 3, '0000-00-00 00:00:00', '::1', '21:24:48', 0),
(7, 3, '0000-00-00 00:00:00', '::1', '22:19:22', NULL),
(8, 19, '0000-00-00 00:00:00', '::1', '22:21:09', 0),
(9, 3, '0000-00-00 00:00:00', '::1', '20:53:49', NULL),
(10, 3, '0000-00-00 00:00:00', '::1', '00:02:55', 0),
(11, 3, '0000-00-00 00:00:00', '::1', '00:03:35', 0),
(12, 3, '0000-00-00 00:00:00', '::1', '00:04:08', 0),
(13, 3, '0000-00-00 00:00:00', '::1', '01:13:40', NULL),
(14, 26, '0000-00-00 00:00:00', '::1', '01:14:21', 0),
(15, 3, '0000-00-00 00:00:00', '::1', '01:15:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rjesenja`
--

CREATE TABLE `rjesenja` (
  `id` int(11) NOT NULL,
  `zadatak_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rjesenje` longtext COLLATE utf8mb4_unicode_ci,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `bodovi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rjesenja`
--

INSERT INTO `rjesenja` (`id`, `zadatak_id`, `user_id`, `rjesenje`, `timestamp`, `bodovi`) VALUES
(1, 1, 26, 'sdfsfslafkasfčlaskfd', '2017-01-04 00:00:48', 0),
(2, 1, 26, 'sdfsfslafkasfčlaskfd', '2017-01-04 00:01:50', 0),
(3, 1, 26, 'sadfasdfsadf', '2017-01-04 00:02:25', 0),
(4, 1, 26, 'asdfasdfasdfas', '2017-01-04 00:03:03', 0),
(5, 1, 26, '#include<cstdio>\nint main(){\n	int n, i, s=0, b=0;\n	scanf("%d", &n);\n	for(i=999; i > 11; i--){\n		if ( i%16 == 0){\n			s+=i;\n			b++;\n			printf("%d\\n", i);\n		}\n		if ( b == 3) \n			break;\n	}\n	printf("%d\\n ", s);\n	return 0;\n}', '2017-01-04 00:06:15', 8),
(6, 1, 26, '#include<cstdio>\nint main(){\n	int n, i, s=0, b=0;\n	scanf("%d", &n);\n	for(i=999; i > 11; i--){\n		if ( i%16 == 0){\n			s+=i;\n			b++;\n			printf("%d\\n", i);\n		}\n		if ( b == 3) \n			break;\n	}\n	printf("%d\\n ", s);\n	return 0;\n}', '2017-01-04 00:06:27', 10),
(7, 1, 26, '#include<cstdio>\nint main(){\n	int n, i, s=0, b=0;\n	scanf("%d", &n);\n	for(i=999; i > 11; i--){\n		if ( i%16 == 0){\n			s+=i;\n			b++;\n			printf("%d\\n", i);\n		}\n		if ( b == 3) \n			break;\n	}\n	printf("%d\\n ", s);\n	return 0;\n}', '2017-01-04 01:20:44', 10),
(8, 1, 26, '#include <cstdio>\nint main(){\n	int n, i, s=0, b=0;\n	scanf("%d", &n);\n	for(i=999; i > 11; i--){\n		if ( i%16 == 0){\n			s+=i;\n			b++;\n			printf("%d\\n", i);\n		}\n		if ( b == 3) \n			break;\n	}\n	printf("%d\\n ", s);\n	return 0;\n}', '2017-01-04 01:33:15', 10),
(9, 2, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid drawT(int);\n\nint main(void)\n{\n	int n;\n	scanf("%d", &n);\n	drawT(n);\n	return EXIT_SUCCESS;\n}\n\nvoid drawT(int n)\n{\n	for(int i = 1; i <= n; i++)\n	{\n		for(int j = 1; j <= n; j++)\n		{\n			if(i == 1 || j == n / 2 + 1) printf("t");\n			else printf(".");\n		}\n		printf("\\n");\n	}\n}', '2017-01-04 01:33:30', 0),
(10, 3, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid draw1(int);\nvoid draw3(int);\nvoid draw7(int);\n\nint main(void)\n{\n    int x, h;\n    scanf("%d%d", &x, &h);\n    if(x == 1)\n    {\n        draw1(h);\n    }\n    if(x == 3)\n    {\n        draw3(h);\n    }\n    if(x == 7)\n    {\n        draw7(h);\n    }\n    return EXIT_SUCCESS;\n}\n\nvoid draw1(int h)\n{\n    for(; h != 0; h--)          //  Ovako mozete koristi for ako je iterator\n    {                           //  vec poznat (u ovom slucaju iskoristi mozemo h)\n        printf("   *\\n");\n    }\n}\n\nvoid draw3(int h)\n{\n    int first = h;\n    int middle = h/2 + 1;\n    for(; h != 0; h--)\n    {\n        if(h == first || h == middle || h == 1) printf("****\\n");\n        else printf("   *\\n");\n    }\n}\n\nvoid draw7(int h)\n{\n    int first = h;\n    for(; h != 0; h--)\n    {\n        if(h == first) printf("****\\n");\n        else printf("   *\\n");\n    }\n}', '2017-01-04 01:34:05', 10),
(11, 4, 26, '#include <cstdio>\n#include <cstdlib>\n\ndouble rootOf(double, double);\ndouble abs(double);\n\nint main(void)\n{\n	double a;\n	double preciznost = 0.0001;\n	scanf("%lf" , &a);\n	printf("%lf\\n", rootOf(a,preciznost));\n	return EXIT_SUCCESS;\n}\n\ndouble abs(double a)\n{\n	return a > 0? a : -a;\n}\n\ndouble rootOf(double a, double preciznost)\n{\n	double gornja = a;\n	double donja = 1;\n	double srednja = (gornja+donja)/2;\n\n	while(abs(srednja * srednja - a) >= preciznost)\n	{\n		if(srednja * srednja > a)\n		{\n			gornja = srednja;\n			srednja = (gornja + donja)/2;\n		}\n		else\n		{\n			donja = srednja;\n			srednja = (gornja + donja)/2;\n		}\n	}\n	return srednja;\n}', '2017-01-04 01:34:44', 10),
(12, 2, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid drawT(int);\n\nint main(void)\n{\n	int n;\n	scanf("%d", &n);\n	drawT(n);\n	return EXIT_SUCCESS;\n}\n\nvoid drawT(int n)\n{\n	for(int i = 1; i <= n; i++)\n	{\n		for(int j = 1; j <= n; j++)\n		{\n			if(i == 1 || j == n / 2 + 1) printf("t");\n			else printf(".");\n		}\n		printf("\\n");\n	}\n}', '2017-01-04 01:34:53', 0),
(13, 2, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid drawT(int);\n\nint main(void)\n{\n	int n;\n	scanf("%d", &n);\n	drawT(n);\n	return EXIT_SUCCESS;\n}\n\nvoid drawT(int n)\n{\n	for(int i = 1; i <= n; i++)\n	{\n		for(int j = 1; j <= n; j++)\n		{\n			if(i == 1 || j == n / 2 + 1) printf("t");\n			else printf(".");\n		}\n		printf("\\n");\n	}\n}', '2017-01-04 01:38:21', 0),
(14, 2, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid drawT(int);\n\nint main(void)\n{\n	int n;\n	scanf("%d", &n);\n	drawT(n);\n	return EXIT_SUCCESS;\n}\n\nvoid drawT(int n)\n{\n	for(int i = 1; i <= n; i++)\n	{\n		for(int j = 1; j <= n; j++)\n		{\n			if(i == 1 || j == n / 2 + 1) printf("t");\n			else printf(".");\n		}\n		printf("\\n");\n	}\n}', '2017-01-04 01:42:44', 9),
(15, 1, 26, '#include <cstdio>\nint main(){\n	int n, i, s=0, b=0;\n	scanf("%d", &n);\n	for(i=999; i > 11; i--){\n		if ( i%16 == 0){\n			s+=i;\n			b++;\n			printf("%d\\n", i);\n		}\n		if ( b == 3) \n			break;\n	}\n	printf("%d\\n ", s);\n	return 0;\n}', '2017-01-04 01:42:52', 10),
(16, 3, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid draw1(int);\nvoid draw3(int);\nvoid draw7(int);\n\nint main(void)\n{\n    int x, h;\n    scanf("%d%d", &x, &h);\n    if(x == 1)\n    {\n        draw1(h);\n    }\n    if(x == 3)\n    {\n        draw3(h);\n    }\n    if(x == 7)\n    {\n        draw7(h);\n    }\n    return EXIT_SUCCESS;\n}\n\nvoid draw1(int h)\n{\n    for(; h != 0; h--)          //  Ovako mozete koristi for ako je iterator\n    {                           //  vec poznat (u ovom slucaju iskoristi mozemo h)\n        printf("   *\\n");\n    }\n}\n\nvoid draw3(int h)\n{\n    int first = h;\n    int middle = h/2 + 1;\n    for(; h != 0; h--)\n    {\n        if(h == first || h == middle || h == 1) printf("****\\n");\n        else printf("   *\\n");\n    }\n}\n\nvoid draw7(int h)\n{\n    int first = h;\n    for(; h != 0; h--)\n    {\n        if(h == first) printf("****\\n");\n        else printf("   *\\n");\n    }\n}', '2017-01-04 01:43:01', 10),
(17, 4, 26, '#include <cstdio>\n#include <cstdlib>\n\ndouble rootOf(double, double);\ndouble abs(double);\n\nint main(void)\n{\n	double a;\n	double preciznost = 0.0001;\n	scanf("%lf" , &a);\n	printf("%lf\\n", rootOf(a,preciznost));\n	return EXIT_SUCCESS;\n}\n\ndouble abs(double a)\n{\n	return a > 0? a : -a;\n}\n\ndouble rootOf(double a, double preciznost)\n{\n	double gornja = a;\n	double donja = 1;\n	double srednja = (gornja+donja)/2;\n\n	while(abs(srednja * srednja - a) >= preciznost)\n	{\n		if(srednja * srednja > a)\n		{\n			gornja = srednja;\n			srednja = (gornja + donja)/2;\n		}\n		else\n		{\n			donja = srednja;\n			srednja = (gornja + donja)/2;\n		}\n	}\n	return srednja;\n}', '2017-01-04 01:43:10', 9),
(18, 4, 26, '#include <cstdio>\n#include <cstdlib>\n\ndouble rootOf(double, double);\ndouble abs(double);\n\nint main(void)\n{\n	double a;\n	double preciznost = 0.0001;\n	scanf("%lf" , &a);\n	printf("%lf\\n", rootOf(a,preciznost));\n	return EXIT_SUCCESS;\n}\n\ndouble abs(double a)\n{\n	return a > 0? a : -a;\n}\n\ndouble rootOf(double a, double preciznost)\n{\n	double gornja = a;\n	double donja = 1;\n	double srednja = (gornja+donja)/2;\n\n	while(abs(srednja * srednja - a) >= preciznost)\n	{\n		if(srednja * srednja > a)\n		{\n			gornja = srednja;\n			srednja = (gornja + donja)/2;\n		}\n		else\n		{\n			donja = srednja;\n			srednja = (gornja + donja)/2;\n		}\n	}\n	return srednja;\n}', '2017-01-04 01:43:15', 10),
(19, 1, 26, '#include <cstdio>\nint main(){\n	int n, i, s=0, b=0;\n	scanf("%d", &n);\n	for(i=999; i > 11; i--){\n		if ( i%16 == 0){\n			s+=i;\n			b++;\n			printf("%d\\n", i);\n		}\n		if ( b == 3) \n			break;\n	}\n	printf("%d\\n ", s);\n	return 0;\n}', '2017-01-04 01:58:15', 10),
(20, 2, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid drawT(int);\n\nint main(void)\n{\n	int n;\n	scanf("%d", &n);\n	drawT(n);\n	return EXIT_SUCCESS;\n}\n\nvoid drawT(int n)\n{\n	for(int i = 1; i <= n; i++)\n	{\n		for(int j = 1; j <= n; j++)\n		{\n			if(i == 1 || j == n / 2 + 1) printf("t");\n			else printf(".");\n		}\n		printf("\\n");\n	}\n}', '2017-01-04 01:58:26', 9),
(21, 3, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid draw1(int);\nvoid draw3(int);\nvoid draw7(int);\n\nint main(void)\n{\n    int x, h;\n    scanf("%d%d", &x, &h);\n    if(x == 1)\n    {\n        draw1(h);\n    }\n    if(x == 3)\n    {\n        draw3(h);\n    }\n    if(x == 7)\n    {\n        draw7(h);\n    }\n    return EXIT_SUCCESS;\n}\n\nvoid draw1(int h)\n{\n    for(; h != 0; h--)          //  Ovako mozete koristi for ako je iterator\n    {                           //  vec poznat (u ovom slucaju iskoristi mozemo h)\n        printf("   *\\n");\n    }\n}\n\nvoid draw3(int h)\n{\n    int first = h;\n    int middle = h/2 + 1;\n    for(; h != 0; h--)\n    {\n        if(h == first || h == middle || h == 1) printf("****\\n");\n        else printf("   *\\n");\n    }\n}\n\nvoid draw7(int h)\n{\n    int first = h;\n    for(; h != 0; h--)\n    {\n        if(h == first) printf("****\\n");\n        else printf("   *\\n");\n    }\n}', '2017-01-04 01:58:35', 9),
(22, 3, 26, '#include <cstdlib>\n#include <cstdio>\n\nvoid draw1(int);\nvoid draw3(int);\nvoid draw7(int);\n\nint main(void)\n{\n    int x, h;\n    scanf("%d%d", &x, &h);\n    if(x == 1)\n    {\n        draw1(h);\n    }\n    if(x == 3)\n    {\n        draw3(h);\n    }\n    if(x == 7)\n    {\n        draw7(h);\n    }\n    return EXIT_SUCCESS;\n}\n\nvoid draw1(int h)\n{\n    for(; h != 0; h--)          //  Ovako mozete koristi for ako je iterator\n    {                           //  vec poznat (u ovom slucaju iskoristi mozemo h)\n        printf("   *\\n");\n    }\n}\n\nvoid draw3(int h)\n{\n    int first = h;\n    int middle = h/2 + 1;\n    for(; h != 0; h--)\n    {\n        if(h == first || h == middle || h == 1) printf("****\\n");\n        else printf("   *\\n");\n    }\n}\n\nvoid draw7(int h)\n{\n    int first = h;\n    for(; h != 0; h--)\n    {\n        if(h == first) printf("****\\n");\n        else printf("   *\\n");\n    }\n}', '2017-01-04 01:58:39', 10),
(23, 4, 26, 'asdfasdf', '2017-01-04 01:58:43', 0),
(24, 1, 26, '#include <cstdio>\nint main(){\n	int n, i, s=0, b=0;\n	scanf("%d", &n);\n	for(i=999; i > 11; i--){\n		if ( i%16 == 0){\n			s+=i;\n			b++;\n			printf("%d\\n", i);\n		}\n		if ( b == 3) \n			break;\n	}\n	printf("%d\\n ", s);\n	return 0;\n}', '2017-01-04 10:31:44', 9),
(25, 1, 26, 'dd', '2017-01-28 15:24:35', 0),
(26, 1, 26, 'asdf', '2017-01-29 14:53:31', 0),
(27, 1, 26, '<i class="glyphicon glyphicon-refresh spinning"></i>', '2017-01-29 14:54:20', 0),
(28, 1, 26, '<i class="glyphicon glyphicon-refresh spinning"></i>', '2017-01-29 14:55:43', 0),
(29, 1, 26, '<i class="glyphicon glyphicon-refresh spinning"></i>', '2017-01-29 14:56:30', 0),
(30, 1, 26, '<i class="glyphicon glyphicon-refresh spinning"></i>', '2017-01-29 14:56:34', 0),
(31, 1, 26, '#include <cstdlib>\n#include <cstdio>\n\nint main(void)\n{\nfor(;;)\n{\n}\n}', '2017-01-29 14:57:05', 0),
(32, 1, 26, '#include <cstdlib>\n#include <cstdio>\n\nint main(void)\n{\nfor(;;)\n{\n}\n}', '2017-01-29 14:57:12', 0),
(33, 1, 26, 'span', '2017-01-29 14:58:05', 0),
(34, 1, 26, 'span', '2017-01-29 14:58:06', 0),
(35, 1, 26, 'span', '2017-01-29 14:58:08', 0),
(36, 1, 26, 'span', '2017-01-29 14:58:11', 0),
(37, 1, 26, 'span', '2017-01-29 14:58:14', 0),
(38, 1, 26, 'span', '2017-01-29 14:58:16', 0),
(39, 1, 26, 'span', '2017-01-29 14:58:40', 0),
(40, 1, 26, 'span', '2017-01-29 14:58:42', 0),
(41, 1, 26, 'span', '2017-01-29 14:58:44', 0),
(42, 1, 26, 'span', '2017-01-29 14:58:46', 0),
(43, 1, 26, 'span', '2017-01-29 14:58:48', 0),
(44, 1, 26, 'span', '2017-01-29 14:58:51', 0),
(45, 1, 26, 'asdfsdfasdf', '2017-01-29 15:02:18', 0),
(46, 1, 26, 'asdfsdfasdf', '2017-01-29 15:02:21', 0),
(47, 1, 26, 'asdfsdfasdf', '2017-01-29 15:02:49', 0),
(48, 1, 26, 'asdfsdfasdf', '2017-01-29 15:02:51', 0),
(49, 1, 26, 'asdfsdfasdf', '2017-01-29 15:02:58', 0),
(50, 1, 26, 'asdfsdfasdf', '2017-01-29 15:03:06', 0),
(51, 1, 26, 'asdfsdfasdf', '2017-01-29 15:03:08', 0),
(52, 1, 26, 'asdfsdfasdf', '2017-01-29 15:03:10', 0),
(53, 1, 26, 'asdfsdfasdf', '2017-01-29 15:03:12', 0),
(54, 1, 26, 'asdfsdfasdf', '2017-01-29 15:03:14', 0),
(55, 1, 26, 'asdfsdfasdf', '2017-01-29 15:03:15', 0),
(56, 1, 26, 'asdfsdfasdf', '2017-01-29 15:03:17', 0),
(57, 1, 26, '#include <cstdio>\n#include <cstdlbi>\n\nsdfdf\n{\n\n    \n}', '2017-01-29 17:09:30', 0),
(58, 2, 26, 'jkokpkppkokopkopkopkop', '2017-01-29 17:26:10', 0),
(59, 2, 26, '#include <cstdlib>\r\n#include <cstdio>\r\n\r\nvoid drawT(int);\r\n\r\nint main(void)\r\n{\r\n	int n;\r\n	scanf("%d", &n);\r\n	drawT(n);\r\n	return EXIT_SUCCESS;\r\n}\r\n\r\nvoid drawT(int n)\r\n{\r\n	for(int i = 1; i <= n; i++)\r\n	{\r\n		for(int j = 1; j <= n; j++)\r\n		{\r\n			if(i == 1 || j == n / 2 + 1) printf("t");\r\n			else printf(".");\r\n		}\r\n		printf("\\n");\r\n	}\r\n}', '2017-01-29 17:28:49', 6),
(60, 2, 26, '#include <cstdlib>\r\n#include <cstdio>\r\n\r\nvoid drawT(int);\r\n\r\nint main(void)\r\n{\r\n	int n;\r\n	scanf("%d", &n);\r\n	drawT(n);\r\n	return EXIT_SUCCESS;\r\n}\r\n\r\nvoid drawT(int n)\r\n{\r\n	for(int i = 1; i <= n; i++)\r\n	{\r\n		for(int j = 1; j <= n; j++)\r\n		{\r\n			if(i == 1 || j == n / 2 + 1) printf("t");\r\n			else printf(".");\r\n		}\r\n		printf("\\n");\r\n	}\r\n}\r\n', '2017-01-29 17:29:12', 8),
(61, 2, 26, '#include <cstdlib>\r\n#include <cstdio>\r\n\r\nvoid drawT(int);\r\n\r\nint main(void)\r\n{\r\n	int n;\r\n	scanf("%d", &n);\r\n	drawT(n);\r\n	return EXIT_SUCCESS;\r\n}\r\n\r\nvoid drawT(int n)\r\n{\r\n	for(int i = 1; i <= n; i++)\r\n	{\r\n		for(int j = 1; j <= n; j++)\r\n		{\r\n			if(i == 1 || j == n / 2 + 1) printf("t");\r\n			else printf(".");\r\n		}\r\n		printf("\\n");\r\n	}\r\n}\r\n', '2017-01-29 17:29:18', 8),
(62, 2, 26, '#include <cstdlib>\r\n#include <cstdio>\r\n\r\nvoid drawT(int);\r\n\r\nint main(void)\r\n{\r\n	int n;\r\n	scanf("%d", &n);\r\n	drawT(n);\r\n	return EXIT_SUCCESS;\r\n}\r\n\r\nvoid drawT(int n)\r\n{\r\n	for(int i = 1; i <= n; i++)\r\n	{\r\n		for(int j = 1; j <= n; j++)\r\n		{\r\n			if(i == 1 || j == n / 2 + 1) printf("t");\r\n			else printf(".");\r\n		}\r\n		printf("\\n");\r\n	}\r\n}\r\n', '2017-01-29 17:29:23', 8),
(63, 1, 26, 'asdf', '2017-01-29 17:31:14', 0),
(64, 1, 26, 'asdf', '2017-01-29 17:31:17', 0),
(65, 1, 26, 'asdfsadfasdfs', '2017-01-29 17:31:19', 0),
(66, 1, 26, 'asdfsadfasdfs', '2017-01-29 17:31:21', 0),
(67, 1, 26, 'asdfsadfasdfs', '2017-01-29 17:31:23', 0),
(68, 2, 26, '#include <cstdlib>\r\n#include <cstdio>\r\n\r\nvoid drawT(int);\r\n\r\nint main(void)\r\n{\r\n	int n;\r\n	scanf("%d", &n);\r\n	drawT(n);\r\n	return EXIT_SUCCESS;\r\n}\r\n\r\nvoid drawT(int n)\r\n{\r\n	for(int i = 1; i <= n; i++)\r\n	{\r\n		for(int j = 1; j <= n; j++)\r\n		{\r\n			if(i == 1 || j == n / 2 + 1) printf("t");\r\n			else printf(".");\r\n		}\r\n		printf("\\n");\r\n	}\r\n}', '2017-01-29 17:33:53', 8),
(69, 1, 26, '#include <cstdio>\n#include <cstdlib>\n\nint main(void)\n{\n    \n}', '2017-01-30 16:12:19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `testovi`
--

CREATE TABLE `testovi` (
  `id` int(11) NOT NULL,
  `naziv` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testovi`
--

INSERT INTO `testovi` (`id`, `naziv`) VALUES
(1, 'Primjer Testa'),
(2, 'Test'),
(3, 'F'),
(4, 'F'),
(5, 'abs'),
(7, 'Test broj jedan'),
(8, 'test2131'),
(10, '789'),
(11, 'dobar dan'),
(12, 'dobar dan');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ime` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prezime` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razred` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `superuser` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ime`, `prezime`, `password`, `razred`, `superuser`) VALUES
(3, 'admin', 'admin', 'admin123', '', 1),
(4, 'Luka', 'Vuletić', '19.10.1999.', '2014c', NULL),
(5, 'Majda', 'Vartušek', '26.08.1999.', '2014c', NULL),
(6, 'Borna', 'Svetinović', '11.07.1999.', '2014c', NULL),
(7, 'Mate', 'Šakić', '03.05.1999.', '2014c', NULL),
(8, 'Antonio', 'Stipić', '15.10.1999.', '2014c', NULL),
(9, 'Saša', 'Milaković', '14.02.1999.', '2014c', NULL),
(10, 'Marina', 'Marić', '13.05.1999.', '2014c', NULL),
(11, 'Karla', 'Lauš', '22.05.1999.', '2014c', NULL),
(12, 'Vanja', 'Grabovac', '04.11.1999.', '2014c', NULL),
(13, 'Matej', 'Dobrošević', '31.07.1999.', '2014c', NULL),
(14, 'Janko', 'Čivić', '18.11.1999.', '2014c', NULL),
(15, 'Filip', 'Bodražić', '20.06.1999.', '2014c', NULL),
(16, 'Bruno', 'Walz', '26.11.1999.', '2014c', NULL),
(17, 'Krunoslav', 'Vrkljan', '10.08.1999.', '2014c', NULL),
(18, 'Dario', 'Vavra', '17.01.2000.', '2014c', NULL),
(19, 'Dunja', 'Šmigovec', '27.04.2000.', '2014c', NULL),
(20, 'Daniel', 'Šileš', '08.04.1999.', '2014c', NULL),
(21, 'Gabriela', 'Peranović', '17.04.2000.', '2014c', NULL),
(22, 'Luka', 'Marjanović', '06.06.1999.', '2014c', NULL),
(23, 'Petra', 'Križevac', '22.12.1999.', '2014c', NULL),
(24, 'Petra', 'Kristić', '15.06.2000.', '2014c', NULL),
(25, 'Petar', 'Ivić', '21.12.1999.', '2014c', NULL),
(26, 'Dino', 'Grgić', '25.10.1999.', '2014c', NULL),
(27, 'Robert', 'Glavaš', '23.05.1999.', '2014c', NULL),
(28, 'Ivan', 'Dulić', '27.04.1999.', '2014c', NULL),
(29, 'Jana', 'Dukić', '01.09.1999.', '2014c', NULL),
(30, 'Ivor', 'Dukić', '22.12.1999.', '2014c', NULL),
(31, 'Lovro', 'Borić', '13.08.1999.', '2014c', NULL),
(32, 'Dorian', 'Tatarin', '18.04.1999.', '2014d', NULL),
(33, 'Karla', 'Šarac', '05.11.1999.', '2014d', NULL),
(34, 'Matija', 'Patajac', '26.07.1999.', '2014d', NULL),
(35, 'Vjekoslav', 'Ormanac', '11.10.1999.', '2014d', NULL),
(36, 'Ema', 'Miličević', '17.09.1999.', '2014d', NULL),
(37, 'Petra', 'Marjanović', '06.10.1999.', '2014d', NULL),
(38, 'Karlo', 'Marjanac', '21.09.1999.', '2014d', NULL),
(39, 'Lara', 'Krstanović', '13.11.1999.', '2014d', NULL),
(40, 'Mateja', 'Krišto', '11.01.2000.', '2014d', NULL),
(41, 'Ana', 'Koturić', '11.11.1999.', '2014d', NULL),
(42, 'Marta', 'Keglević', '04.08.1999.', '2014d', NULL),
(43, 'Josip', 'Kajan', '27.04.1999.', '2014d', NULL),
(44, 'Patrik', 'Jusup', '26.11.1999.', '2014d', NULL),
(45, 'Branimir', 'Ivić', '01.05.1999.', '2014d', NULL),
(46, 'Luka', 'Hornung', '12.01.1999.', '2014d', NULL),
(47, 'Matej', 'Eršetić', '20.12.1999.', '2014d', NULL),
(48, 'Roko', 'Erceg', '06.10.1999.', '2014d', NULL),
(49, 'Franko', 'Cebić', '30.12.1999.', '2014d', NULL),
(50, 'Klara', 'Žuljević', '23.10.1999.', '2014a', NULL),
(51, 'Cinderella', 'Talpai', '07.08.1999.', '2014a', NULL),
(52, 'Mislav', 'Štiglec', '17.05.1999.', '2014a', NULL),
(53, 'Laura', 'Šoja', '30.06.1999.', '2014a', NULL),
(54, 'Agneza', 'Rukavina', '24.04.2000.', '2014a', NULL),
(55, 'Ivan', 'Petrinšak', '17.04.1999.', '2014a', NULL),
(56, 'Matija', 'Paris', '26.01.2000.', '2014a', NULL),
(57, 'Lucija', 'Nako', '27.09.1999.', '2014a', NULL),
(58, 'Vlatka', 'Mihić', '25.03.2000.', '2014a', NULL),
(59, 'Josipa', 'Lendić', '20.03.2000.', '2014a', NULL),
(60, 'Helena', 'Ladić', '07.11.1999.', '2014a', NULL),
(61, 'Petra', 'Knežević', '25.12.1999.', '2014a', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zadatci`
--

CREATE TABLE `zadatci` (
  `id` int(11) NOT NULL,
  `naziv` text COLLATE utf8mb4_unicode_ci,
  `tekst` longtext COLLATE utf8mb4_unicode_ci,
  `ins` text COLLATE utf8mb4_unicode_ci,
  `outs` text COLLATE utf8mb4_unicode_ci,
  `test_id` int(11) DEFAULT NULL,
  `broj_zadatka` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `zadatci`
--

INSERT INTO `zadatci` (`id`, `naziv`, `tekst`, `ins`, `outs`, `test_id`, `broj_zadatka`) VALUES
(1, 'Zid', 'Zidamo zid od blokova dugacak M metara. Imamo broj malih blokova (od 1m) i velikih blokova (od 5m). Napiši program koji pomocu funkcije provjerava moze li se raspoloživim blokovima sagraditi zid zeljene duzine.\r\n\r\nULAZNI PODACI\r\n- tri broja: broj malih blokova, broj velikih blokova, zeljena duzina zida\r\n\r\nIZLAZNI PODACI\r\n- 1 ako je moguce sagraditi zid, 0 ako nije moguce\r\nžžžžžžž', '1000,202,303,404,505,606,707,808,909,123', '992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ,992\r\n976\r\n960\r\n2928\r\n ', 1, 1),
(2, 'Draw_T', 'Nacrtaj slovo T sa zvjezdicama', '1,2,3,4,5,6,7,8,9', 't\r\n,tt\r\n.t\r\n,ttt\r\n.t.\r\n.t.\r\n,tttt\r\n..t.\r\n..t.\r\n..t.\r\n,ttttt\r\n..t..\r\n..t..\r\n..t..\r\n..t..\r\n,tttttt\r\n...t..\r\n...t..\r\n...t..\r\n...t..\r\n...t..\r\n,ttttttt\r\n...t...\r\n...t...\r\n...t...\r\n...t...\r\n...t...\r\n...t...\r\n,tttttttt\r\n....t...\r\n....t...\r\n....t...\r\n....t...\r\n....t...\r\n....t...\r\n....t...\r\n,ttttttttt\r\n....t....\r\n....t....\r\n....t....\r\n....t....\r\n....t....\r\n....t....\r\n....t....\r\n....t....\r\n', 1, 2),
(3, 'Draw137', 'Nacrtaj brojeve 1, 3 i 7 sa zvjezdicama tako da se u prvi red unosi broj koji se ispisuje a u drugi red koliko će taj broj imati zvjezdica.', '1 3,1 4,1 5,3 6,3 7,3 5,3 2,7 1,7 9,7 12', '   *\r\n   *\r\n   *\r\n,   *\r\n   *\r\n   *\r\n   *\r\n,   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n,****\r\n   *\r\n****\r\n   *\r\n   *\r\n****\r\n,****\r\n   *\r\n   *\r\n****\r\n   *\r\n   *\r\n****\r\n,****\r\n   *\r\n****\r\n   *\r\n****\r\n,****\r\n****\r\n,****\r\n,****\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n,****\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n   *\r\n', 1, 3),
(4, 'Drugi_Korijen', 'Napiši zadatak koji će ispisivati drugi korijen unesenog broja n bez korištenja math librarya.', '1,2,3,4,5,6,7,8,9,10', '1.000000\r\n,1.414185\r\n,1.732056\r\n,1.999985\r\n,2.236084\r\n,2.449509\r\n,2.645752\r\n,2.828426\r\n,3.000000\r\n,3.162281\r\n', 1, 4),
(8, 'Teest', 'test', 'test', 't,tt.t,ttt.t..t.,tttt..t...t...t.,ttttt..t....t....t....t..,tttttt...t.....t.....t.....t.....t..,ttttttt...t......t......t......t......t......t...,tttttttt....t.......t.......t.......t.......t.......t.......t...,ttttttttt....t........t........t........t........t........t........t........t....,tttttttttt.....t.........t.........t.........t.........t.........t.........t.........t.........t....', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ispiti`
--
ALTER TABLE `ispiti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rjesenja`
--
ALTER TABLE `rjesenja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testovi`
--
ALTER TABLE `testovi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zadatci`
--
ALTER TABLE `zadatci`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ispiti`
--
ALTER TABLE `ispiti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `rjesenja`
--
ALTER TABLE `rjesenja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `testovi`
--
ALTER TABLE `testovi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `zadatci`
--
ALTER TABLE `zadatci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
